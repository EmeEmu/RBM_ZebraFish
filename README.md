<!-- <1!-- PROJECT LOGO --1> -->
<!-- <br /> -->
<!-- <p align="center"> -->
<!--   <a href="https://github.com/othneildrew/Best-README-Template"> -->
<!--     <img src="images/logo.png" alt="Logo" width="80" height="80"> -->
<!--   </a> -->

<!--   <h3 align="center">Best-README-Template</h3> -->

<!--   <p align="center"> -->
<!--     An awesome README template to jumpstart your projects! -->
<!--     <br /> -->
<!--     <a href="https://github.com/othneildrew/Best-README-Template"><strong>Explore the docs »</strong></a> -->
<!--     <br /> -->
<!--     <br /> -->
<!--     <a href="https://github.com/othneildrew/Best-README-Template">View Demo</a> -->
<!--     · -->
<!--     <a href="https://github.com/othneildrew/Best-README-Template/issues">Report Bug</a> -->
<!--     · -->
<!--     <a href="https://github.com/othneildrew/Best-README-Template/issues">Request Feature</a> -->
<!--   </p> -->
<!-- </p> -->



<!-- <1!-- TABLE OF CONTENTS --1> -->
<!-- <details open="open"> -->
<!--   <summary>Table of Contents</summary> -->
<!--   <ol> -->
<!--     <li> -->
<!--       <a href="#about-the-project">About The Project</a> -->
<!--       <ul> -->
<!--         <li><a href="#built-with">Built With</a></li> -->
<!--       </ul> -->
<!--     </li> -->
<!--     <li> -->
<!--       <a href="#getting-started">Getting Started</a> -->
<!--       <ul> -->
<!--         <li><a href="#prerequisites">Prerequisites</a></li> -->
<!--         <li><a href="#installation">Installation</a></li> -->
<!--       </ul> -->
<!--     </li> -->
<!--     <li><a href="#usage">Usage</a></li> -->
<!--     <li><a href="#roadmap">Roadmap</a></li> -->
<!--     <li><a href="#contributing">Contributing</a></li> -->
<!--     <li><a href="#license">License</a></li> -->
<!--     <li><a href="#contact">Contact</a></li> -->
<!--     <li><a href="#acknowledgements">Acknowledgements</a></li> -->
<!--   </ol> -->
<!-- </details> -->



<!-- ABOUT THE PROJECT -->
## About The Repository

This repository contains tools and methods to use the RBMs from the [Probabilistic Graphical Models (PGM) repository](https://github.com/jertubiana/PGM) written by [Jerome Tubiana](https://github.com/jertubiana).

It should be noted that this repository is curently in very early development, and **I therefore advise that you do not use it for the time being**.


## Installation

The present repository is built on the [PGM repository](https://github.com/jertubiana/PGM) which is included as a git submodule. To clone it you will therefore need the following procedure :

```sh
git clone --recurse-submodules https://gitlab.com/EmeEmu/RBM_ZebraFish
``` 
This will both clone the *RBM\_Zebrafish* repo, and the included *PGM* repo.

## Contributing


## Contact

