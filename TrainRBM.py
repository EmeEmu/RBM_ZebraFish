import argparse

parser = argparse.ArgumentParser()
parser.add_argument(
    "-c",
    "--config_path",
    required=True,
    type=str,
    help="path to yaml configuration file.",
)
parser.add_argument(
    "-nt",
    "--num_threads",
    type=int,
    default=100,
    help="number of threads to use for all RBM computations",
)
args = parser.parse_args()

num_threads = args.num_threads  # Set number of CPUs to use!
config_path = args.config_path  # "./test_config.yaml"

print("SETTING UP")

import json
import os
import pickle
import sys
from datetime import datetime
from pathlib import Path

start = datetime.now()

os.environ["MKL_NUM_THREADS"] = "%s" % num_threads
os.environ["NUMEXPR_NUM_THREADS"] = "%s" % num_threads
os.environ["OMP_NUM_THREADS"] = "%s" % num_threads
os.environ["OPENBLAS_NUM_THREADS"] = "%s" % num_threads
os.environ["VECLIB_MAXIMUM_THREADS"] = "%s" % num_threads
os.environ["NUMBA_NUM_THREADS"] = "%s" % num_threads
sys.path.append("PGM/utilities/")
sys.path.append("PGM/source/")

import numpy as np
import rbm
import yaml

from utils import (make_analysis_montage, plot_hidden_activity,
                   plot_hu_distrib, plot_layer_convergence,
                   plot_learning_curve, plot_stat_convergence,
                   plot_weight_distrib, plot_weight_matrix, pretty)

# loadind parameters from config file
with open(config_path, "r") as config_file:
    conf = yaml.load(config_file, Loader=yaml.FullLoader)

print("\nPREPARING OUTPUT PATHS")
temp_dir = conf["paths"]["main_out"] + conf["paths"]["temp_dir"]
out_paths = [conf["paths"]["main_out"], temp_dir]
for d in out_paths:
    path = Path(d)
    if not path.exists():
        print(f"creating {path}/")
        path.mkdir()
name = "RBM_" + conf["name"] + "_" + start.strftime("%Y%m%d_%H%M%S")
outpath = conf["paths"]["main_out"] + name
conf["paths"]["outpath"] = str(Path(outpath).absolute())

print("\nLOADIND DATA")
spikes = np.load(conf["paths"]["main_in"] + conf["paths"]["spikes_in"]).T
print(f"spikes loaded  -->  shape = {spikes.shape}  (should be [time x neurons])")
n_steps, n_neurons = tuple(spikes.shape)
print(f"\t {n_steps} time steps  |  {n_neurons} neurons")
np.random.shuffle(spikes)
print("spikes shuffled")

ntrain = int(n_steps * conf["dataset_params"]["ptrain"] / 100)
nvalid = int(n_steps * conf["dataset_params"]["pvalid"] / 100)

train_spikes = spikes[0:ntrain]
valid_spikes = spikes[ntrain : ntrain + nvalid]
print(f"loaded {ntrain} ({ntrain/n_steps * 100})% time steps for training.")
print(f"loaded {nvalid} ({nvalid/n_steps * 100})% time steps for validation.")

print("\nSETTING UP RBM")
RBM = rbm.RBM(
    n_v=n_neurons,
    n_h=conf["rbm_params"]["nHU"],
    visible=conf["rbm_params"]["visible_prior"],
    hidden=conf["rbm_params"]["hidden_prior"],
)

print("\nLEARNING STARTED")
start_L = RBM.likelihood(spikes).mean()
print(f"Starting Likelihood = {start_L:0.4f}")

start_fit = datetime.now()
res = RBM.fit(
    train_spikes,
    data_test=valid_spikes,
    learning_rate=conf["train_params"]["lr"],
    lr_final=conf["train_params"]["lr_final"],
    l1=conf["train_params"]["lambda"],
    decay_after=conf["train_params"]["decay_after"],
    optimizer="ADAM",
    extra_params=[0, 0.999, 1e-6],
    vverbose=0,
    verbose=1,
    n_iter=conf["train_params"]["n_iterations"],
    N_MC=conf["train_params"]["n_mcc"],
    record=conf["train_params"]["record_dic"],
    record_interval=conf["train_params"]["record_interval"],
)
stop_fit = datetime.now()

stop_L = RBM.likelihood(spikes, recompute_Z=True).mean()
print(f"Done. Likelihood : {start_L:0.4f} --> {stop_L:0.4f}")
conf["learning"] = {
    "start_Likelihood": start_L,
    "stop_Likelihood": stop_L,
}

print("\nSAVING RBM + LEARNING DATA")
pickle.dump(RBM, open(outpath + ".pickle", "wb"))
pickle.dump(res, open(outpath + "_res.pickle", "wb"))

print("\nGENERATING DATA")
start_gen = datetime.now()
datav, datah = RBM.gen_data(
    Nthermalize=conf["gen_params"]["Nthermalize"],
    Nstep=conf["gen_params"]["Nstep"],
    Nchains=conf["gen_params"]["Nchains"],
    Lchains=conf["gen_params"]["Lchains"],
)
stop_gen = datetime.now()
h_activity = RBM.mean_hiddens(spikes)

print("\nPRODUCING PLOTS")
plot_learning_curve(res, show=False, save=temp_dir, suf="")
plot_layer_convergence(res, show=False, save=temp_dir, suf="")
plot_stat_convergence(RBM, spikes, datav, datah, show=False, save=temp_dir, suf="")
plot_weight_distrib(RBM, show=False, save=temp_dir, suf="")
plot_hu_distrib(h_activity, show=False, save=temp_dir, suf="")
plot_hidden_activity(h_activity, show=False, save=temp_dir, suf="")
plot_weight_matrix(RBM, show=False, save=temp_dir, suf="")


stop = datetime.now()
total_duration = stop - start
fit_duration = stop_fit - start_fit
gen_duration = stop_gen - start_gen
print(
    f"Durations :\n\ttotal : {total_duration}\n\tfit : {fit_duration}\n\tgen : {gen_duration}"
)
conf["durations"] = {
    "total": f"'{total_duration}'",
    "fit": f"'{fit_duration}'",
    "gen": f"'{gen_duration}'",
}

make_analysis_montage(
    temp_dir,
    outpath + "_TandI.png",
    title=conf["name"],
    clean=conf["paths"]["clean"],
    comment=pretty(conf),
)

pickle.dump(conf, open(outpath + ".pickle", "wb"))
print("\nDone.")
