import os

import matplotlib

matplotlib.use("Agg")
import matplotlib.pyplot as plt
import numpy as np
from matplotlib import cm, rc
from scipy import cluster, spatial

# Use LaTeX throughout the figure for consistency
rc("font", **{"family": "serif", "serif": ["Computer Modern"], "size": 16})
rc("text", usetex=True)


def opt_leaf(w_mat, dim=0, link_metric="correlation"):
    """create optimal leaf order over dim, of matrix w_mat. if w_mat is not an
    np.array then its assumed to be a RNN layer.
    see also: https://docs.scipy.org/doc/scipy/reference/generated/scipy.cluster.hierarchy.optimal_leaf_ordering.html#scipy.cluster.hierarchy.optimal_leaf_ordering"""
    # if type(w_mat) != np.ndarray:  # assume it's an rnn layer
    #     w_mat = [x for x in w_mat.parameters()][0].detach().numpy()
    assert w_mat.ndim == 2
    if dim == 1:  # transpose to get right dim in shape
        w_mat = w_mat.T
    dist = spatial.distance.pdist(w_mat, metric=link_metric)  # distanc ematrix
    link_mat = cluster.hierarchy.ward(dist)  # linkage matrix
    if link_metric == "euclidean" and True:
        opt_leaves = cluster.hierarchy.leaves_list(
            cluster.hierarchy.optimal_leaf_ordering(link_mat, dist)
        )
        print("OPTIMAL LEAF SOSRTING AND EUCLIDEAN USED")
    else:
        opt_leaves = cluster.hierarchy.leaves_list(link_mat)
    return opt_leaves, (link_mat, dist)


def plot_learning_curve(res_dic, show=True, save="", suf=""):
    """Plot learning curve from RBM record dirctionary.

    :res: <dic of arrays> RBM output record dictionary.
    :show: <bool> to show the plot.
    :save: <str> path to save plot directory (do not provide file name).
                if "", does not save.
    :suf: <str> suffix to add at the end of the file name.
    """
    fig, axs = plt.subplots(figsize=(16, 6))
    fig.suptitle("Pseudo-Likelihood Learning curves", fontsize=22)

    ax = axs  # pseudo likelyhood
    ax.plot(res_dic["PL"], label="train")
    ax.plot(res_dic["PL_test"], label="test")
    ax.set_xlabel("Training steps", fontsize=22)
    ax.set_ylabel("Pseudo Likelyhood", fontsize=22)
    ax.tick_params(axis="both", labelsize=18)
    ax.legend(fontsize=22)

    if save:
        fig.savefig(save + "TandI_1_learning_curve" + suf + ".png")
    if show:
        plt.show()
    else:
        plt.close("all")


def plot_layer_convergence(res_dic, show=True, save="", suf=""):
    """Plot learning curve of layer parameters from RBM record dirctionary.

    :res: <dic of arrays> RBM output record dictionary.
    :show: <bool> to show the plot.
    :save: <str> path to save plot directory (do not provide file name).
                if "", does not save.
    :suf: <str> suffix to add at the end of the file name.
    """
    params = []
    for r in res_dic:
        if r[0] in ["v", "h"]:
            params.append(r)

    fig, axs = plt.subplots(
        nrows=len(params),
        figsize=(12, 2 * len(params)),
        constrained_layout=True,
        sharex=True,
    )
    fig.suptitle("Layers' Convergence", fontsize=22)

    for i, ax in enumerate(axs):
        ax.plot(res_dic[params[i]])

        x = params[i]
        x = x.replace(x[:2], x[:2].upper())
        x = x.split("_")
        x[1] = "_{" + x[1]
        x[-1] = x[-1] + "}"
        x = " ".join(x)

        ax.set_ylabel("$" + x + "$", fontsize=22)
        ax.tick_params(axis="both", labelsize=18)
    ax.set_xlabel("Training steps", fontsize=22)

    if save:
        fig.savefig(save + "TandI_2_layers_convergence" + suf + ".png")
    if show:
        plt.show()
    else:
        plt.close("all")


def plot_stat_convergence(RBM, spikes, datav, datah, show=True, save="", suf=""):
    """Plot .

    :RBM: <rbm instance> the RBM object.
    :spikes: <2d array> the spiking data.
    :datav: <2d array> RBM generated visible layer activity.
    :datah: <2d array> RBM generated hidden layer activity.
    :show: <bool> to show the plot.
    :save: <str> path to save plot directory (do not provide file name).
                if "", does not save.
    :suf: <str> suffix to add at the end of the file name.
    """

    def sig(ax, x, y, title, xlab, ylab):
        r = np.corrcoef(x.flatten(), y.flatten())[0, 1]
        ax.set_title(title + f"  - r={r:0.2f}", fontsize=22)
        ax.scatter(x, y, color="grey", alpha=0.2)
        ax.plot(
            [np.nanmin(x), np.nanmax(x)],
            [np.nanmin(y), np.nanmax(y)],
            c="orange",
            linewidth=2,
        )
        ax.set_xlabel(xlab, fontsize=22)
        ax.set_ylabel(ylab, fontsize=22)
        ax.tick_params(axis="both", labelsize=20)

    fig, axs = plt.subplots(nrows=3, ncols=2, figsize=(12, 12), constrained_layout=True)
    axs = axs.flatten()
    fig.suptitle("Statistics Convergence", fontsize=24)

    # mean neuron activity
    sig(
        axs[0],
        np.mean(spikes, axis=0),
        np.mean(datav, axis=0),
        r"$\langle v_i \rangle$",
        "data",
        "model",
    )

    # mean hidden units activity
    sig(
        axs[1],
        np.mean(RBM.mean_hiddens(spikes), axis=0),
        np.mean(datah, axis=0),
        r"$\langle h_\mu \rangle$",
        "data",
        "model",
    )

    # neuron pairwise correlation
    sig(
        axs[2],
        np.corrcoef(spikes.T).ravel(),
        np.corrcoef(datav.T).ravel(),
        r"$\langle v_i v_j \rangle$",
        "data",
        "model",
    )

    # <vh>
    mu_vh_data = np.dot(RBM.mean_hiddens(spikes).T, spikes) / len(spikes)
    mu_vh_model = np.dot(RBM.mean_hiddens(datav).T, datav) / len(datav)
    sig(
        axs[3],
        mu_vh_data,
        mu_vh_model,
        r"$\langle v_i h_\mu \rangle$",
        "data",
        "model",
    )

    # <vv> - <v><v>
    X_mean = np.mean(spikes, axis=0)
    X_corr = np.corrcoef(spikes.T)
    X = X_corr - np.outer(X_mean, X_mean)
    Y_mean = np.mean(datav, axis=0)
    Y_corr = np.corrcoef(datav.T)
    Y = Y_corr - np.outer(Y_mean, Y_mean)
    sig(
        axs[4],
        X.ravel(),
        Y.ravel(),
        r"$\langle v_i v_j \rangle - \langle v_i \rangle \langle v_j\rangle$",
        "data",
        "model",
    )

    # <hh> - <h><h>
    X_mean = np.mean(RBM.mean_hiddens(spikes), axis=0)
    X_corr = np.corrcoef(RBM.mean_hiddens(spikes).T)
    X = X_corr - np.outer(X_mean, X_mean)
    Y_mean = np.mean(datah, axis=0)
    Y_corr = np.corrcoef(datah.T)
    Y = Y_corr - np.outer(Y_mean, Y_mean)
    sig(
        axs[5],
        X.ravel(),
        Y.ravel(),
        r"$\langle h_\mu h_\nu \rangle - \langle h_\mu \rangle \langle h_\nu\rangle$",
        "data",
        "model",
    )

    if save:
        fig.savefig(save + "TandI_3_stat_convergence" + suf + ".png")
    if show:
        plt.show()
    else:
        plt.close("all")


def plot_weight_distrib(RBM, show=True, save="", suf=""):
    """Plot distribtution of weight values.

    :RBM: <rbm instance> the RBM object.
    :show: <bool> to show the plot.
    :save: <str> path to save plot directory (do not provide file name).
                if "", does not save.
    :suf: <str> suffix to add at the end of the file name.
    """
    assert np.sum(np.isnan(RBM.weights)) == 0
    fig, ax = plt.subplots(figsize=(10, 5), constrained_layout=True)
    fig.suptitle("Weights' PDF", fontsize=24)
    h = ax.hist(
        RBM.weights.ravel(),
        bins=200,
        density=True,
        histtype="step",
        log=True,
        color="orange",
        linewidth=2,
    )
    ax.set_xlabel("Weight value", fontsize=24)
    ax.set_ylabel("Probability", fontsize=24)
    ax.tick_params(axis="both", labelsize=22)

    if save:
        fig.savefig(save + "TandI_4_weight_distrib" + suf + ".png")
    if show:
        plt.show()
    else:
        plt.close("all")


def plot_hu_distrib(h_activity, show=True, save="", suf=""):
    """Plot distribution of hidden units activity values.

    :h_activity: <2d array> activity of hiden units.
    :show: <bool> to show the plot.
    :save: <str> path to save plot directory (do not provide file name).
                if "", does not save.
    :suf: <str> suffix to add at the end of the file name.
    """
    fig, ax = plt.subplots(figsize=(10, 5), constrained_layout=True)
    fig.suptitle("HUs Activities' PDF", fontsize=24)
    h = ax.hist(
        h_activity.ravel(),
        bins=100,
        density=True,
        histtype="step",
        color="orange",
        linewidth=2,
    )
    ax.set_xlabel("HU values", fontsize=24)
    ax.set_ylabel("Probability", fontsize=24)
    ax.tick_params(axis="both", labelsize=22)

    if save:
        fig.savefig(save + "TandI_5_hu_values" + suf + ".png")
    if show:
        plt.show()
    else:
        plt.close("all")


def plot_hidden_activity(h_activity, show=True, save="", suf=""):
    """Plot activity of all hidden units.

    :h_activity: <2d array> activity of hiden units.
    :show: <bool> to show the plot.
    :save: <str> path to save plot directory (do not provide file name).
                if "", does not save.
    :suf: <str> suffix to add at the end of the file name.
    """
    fig, ax = plt.subplots(figsize=(12, 5), constrained_layout=True)
    fig.suptitle("Hidden Units' Activity", fontsize=24)

    a = ax.imshow(
        h_activity.T,
        aspect="auto",
        cmap="cividis",
        interpolation="nearest",
    )
    c = plt.colorbar(a, ax=ax)
    c.ax.tick_params(labelsize=22)

    ax.set_ylabel("Hiden Units", fontsize=24)
    ax.set_xlabel("Time (s)", fontsize=24)
    c.set_label(label="HU activity", fontsize=24)
    ax.tick_params(axis="both", labelsize=22)

    if save:
        fig.savefig(save + "TandI_6_hu_activity" + suf + ".png")
    if show:
        plt.show()
    else:
        plt.close("all")


def plot_weight_matrix(RBM, show=True, save="", suf=""):
    """Plot the weigh matrix between visible and hidden layers.

    :RBM: <rbm instance> the RBM object.
    :show: <bool> to show the plot.
    :save: <str> path to save plot directory (do not provide file name).
                if "", does not save.
    :suf: <str> suffix to add at the end of the file name.
    """
    fig, ax = plt.subplots(figsize=(12, 5), constrained_layout=True)
    fig.suptitle("Weight Matrix", fontsize=24)

    a = ax.imshow(RBM.weights, aspect="auto", cmap="cividis", interpolation="nearest")
    c = plt.colorbar(a, ax=ax)
    c.ax.tick_params(labelsize=22)

    ax.set_ylabel("Hiden Units", fontsize=24)
    ax.set_xlabel("Neurons", fontsize=24)
    c.set_label(label="Weight value", fontsize=24)
    ax.tick_params(axis="both", labelsize=22)

    if save:
        fig.savefig(save + "TandI_7_weight_matrix" + suf + ".png")
    if show:
        plt.show()
    else:
        plt.close("all")


def make_analysis_montage(
    path_to_plots, path_to_montage, title="", clean=False, comment=""
):
    """Create a montage image from multiple analysis plots.

    :path_to_plots: <str> path to directory where plots are saved.
    :path_to_montage: <str> path (with filename) where the montage should
                            be saved.
    :title: <str> title of the montage.
    :clean: <bool> whether the plot directory should be emptied.
    """
    os.system(
        f"montage {path_to_plots}TandI_* -geometry +50+50 -title '{title}' {path_to_montage}"
    )
    if comment:
        comment = "\n" + comment
        os.system(
            f"convert {path_to_montage} -set comment '{comment}' {path_to_montage}"
        )
    if clean:
        os.system(f"rm {path_to_plots}TandI_*")


def pretty(d, indent=0):
    """Make pretty string from nested dictionary.

    Addapted from : https://stackoverflow.com/a/3229493
    """
    string = ""
    for key, value in d.items():
        string += "\t" * indent + str(key) + "\n"
        if isinstance(value, dict):
            string += pretty(value, indent + 1) + "\n"
        else:
            string += "\t" * (indent + 1) + str(value) + "\n"
    return string
