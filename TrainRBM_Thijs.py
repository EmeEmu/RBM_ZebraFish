## Function to train RBM
## Python 3

#%% General options:
num_threads = 16  # Set number of CPUs to use!

#%% Package loading:
import sys, os, pickle
os.environ["MKL_NUM_THREADS"] = "%s"%num_threads
os.environ["NUMEXPR_NUM_THREADS"] = "%s"%num_threads
os.environ["OMP_NUM_THREADS"] = "%s"%num_threads
os.environ["OPENBLAS_NUM_THREADS"] = "%s"%num_threads
os.environ["VECLIB_MAXIMUM_THREADS"] = "%s"%num_threads
os.environ["NUMBA_NUM_THREADS"] = "%s"%num_threads
import itertools
import numpy as np
import datetime
sys.path.append('PGM3_correct/source/') # the path where the folder PGM is.
sys.path.append('PGM3_correct/utilities/') # the path where the folder PGM is.
import cProfile
import rbm as rbm

def pick_lambda_nhu(iteration):
    """Function that returns lambda and n_HU, for hard-coded arrays, given
    iteration index"""
    assert type(iteration) == int, 'iteration must be an int'
    nhu_arr = np.array([5, 10, 20, 40, 60, 80, 100, 150, 200, 300])
    lambda_arr = np.array([0.005, 0.01, 0.02, 0.05, 0.1, 0.2])
    n_points = (len(nhu_arr) * len(lambda_arr))
    assert (iteration >= 0) and (iteration < n_points), f'iteration must be 0 <= iteration < {n_points}'
    nhu_mesh, lambda_mesh = np.meshgrid(nhu_arr, lambda_arr)  # create 2D mesh
    nhu_mesh = nhu_mesh.ravel()  # flatten
    lambda_mesh = lambda_mesh.ravel()
    assert (lambda_mesh[0] == lambda_mesh[1]) and (nhu_mesh[0] != nhu_mesh[1])  # check that they vary differently
    return lambda_mesh[iteration], nhu_mesh[iteration]

#%%  # Path specification
def train_rbm(parameter_iter, data_path='/home/thijs/Desktop/',
              data_set='20180912_Run01_spontaneous_rbm2__wb_spikes_only.npy',
              rbm_save_path='/home/thijs/Desktop/RBM/For_Thijs/new_sweep/',
              verbose=1, save_rbm=True):
    """Function to train one RBM, with lambda and N_HU specificed by parameter_iter
    (generated from pick_lambda_nhu())."""

    train_inds_path='train_test_inds/20180912-Run01/train_test_inds__test_segs_267_nseg10.pkl'  # HARD SET TO 10TH PERCENTILE OF 20180912-RUN01 (TEST SEGS 267)
    #%% Model training parameters:
    if verbose > 0:
        print('------------------------------------')
    t_start = datetime.datetime.now()
    lambda_param, nhu_param = pick_lambda_nhu(iteration=parameter_iter)  # set lambda and nhu from pre-defined list of combiantions
    number_mcc = 15
    n_updates = 200000
    batch_size = 100
    if parameter == 39:
        lr_param = 1e-3
    else:
        lr_param = 5e-3
    lr_final_param = 1e-5
    decay_after_param = 0.25
    hidden_prior = 'dReLU'
    visible_prior = 'Bernoulli'

    if verbose > 0:
        print(f'Iteration {parameter_iter} with lambda={lambda_param} and nhu={nhu_param}')
        print(t_start)
        print('------------------------------------')

    #%% Data loading
    spikes = np.load(data_path + data_set)  # load data set. These should be a 2D np.array of spikes of (T X N)
    assert spikes.ndim == 2
    assert spikes.shape[0] < spikes.shape[1]  # it should be time x neurons => WORKS ONLY FOR HIGH_DIMENSIONAL ZEBRAFISH DATA
    n_cells = spikes.shape[1]
    dict_tt_inds = pickle.load(open(train_inds_path, 'rb'))  # load dictionary with training indices
    train_inds = dict_tt_inds['train_inds']  # load training inds, note that: # test_inds = dict_tt_inds['test_inds']
    train_spikes = spikes[train_inds, :]  # only use these time frames for training the rbm
    if verbose > 0:
        print(f'Spikes loaded with shape {spikes.shape}')
        print(f'Train inds loaded with shape {train_inds.shape}')
    n_iterations = int(n_updates / (spikes.shape[0] / batch_size))

    #%% Some saving str for later
    rbm_type = 'RBM3'
    data_name = data_set.rstrip('__wb_spikes_only.npy').replace('_', '-')
    str_l1 = '{:.0e}'.format(lambda_param)  # get rid of dot notation
    test_inds_name = train_inds_path.split('__')[1].rstrip('.pkl').replace('_', '-')  # alter format a little

    #%% Train RBM
    RBM = rbm.RBM(n_v=n_cells, n_h=nhu_param, visible=visible_prior, hidden=hidden_prior)  # init rbm
    if verbose > 1:
        rbm_verbose = 1
    else:
        rbm_verbose = 0
    RBM.fit(train_spikes, learning_rate=lr_param, lr_final=lr_final_param, decay_after=decay_after_param,
            optimizer='ADAM', extra_params=[0, 0.999, 1e-6], vverbose=0,
            verbose=rbm_verbose, n_iter=n_iterations, l1=lambda_param, N_MC=number_mcc)  # train with specific parameteers

    #%% Saving names :
    t_end = datetime.datetime.now()
    timestamp = str(t_end.date()) + '-' + str(t_end.hour).zfill(2) + str(t_end.minute).zfill(2)
    duration = int(np.round((t_end - t_start).total_seconds()))
    rbm_name = (f'{rbm_type}_{data_name}_wb_{test_inds_name}_M{nhu_param}_l1-{str_l1}' +
                f'_duration{duration}s_timestamp{timestamp}.data')

    ## Make sure timestamp does not exist yet:
    tmp_count_name_check = 0
    all_current_time_stamps = [x.split('_')[-1].rstrip('.data').lstrip('timestamp') for x in os.listdir(rbm_save_path) if x[-5:] == '.data']
    while timestamp in all_current_time_stamps:  # change time stamp until unique stamp is found
        tmp_count_name_check += 1
        print('changing time stamp')
        timestamp = timestamp[:-4] + str(int(timestamp[-4:]) + 1)  # add one
        rbm_name = (f'{rbm_type}_{data_name}_wb_{test_inds_name}_M{nhu_param}_l1-{str_l1}' +
                    f'_duration{duration}s_timestamp{timestamp}.data')
        if tmp_count_name_check > 20:
            print(f'Something is odd about the rbm name check, now at {tmp_count_name_check} alterations.')
            print('ABORTING')
            break

    #%% Save rbm:
    if verbose > 0:
        print(f'Saving in {rbm_save_path}')
        print(f'Name :{rbm_name}')
    if save_rbm:
        pickle.dump(RBM, open(rbm_save_path + rbm_name, 'wb'))
        if verbose > 0:
            print(f'RBM saved succesfully, total duration: {duration}s.')
    else:
        print('WARNING: RBM WAS NOT SAVED. Set save_rbm=True to save in the future.')

    return RBM

def run_multiple_trainings(start_it=0, n_its=4):
    """Run multipel RBM trainings, where lambda and n_hu are defined by the iteration.
    Start at parameter start_it and increment n_its times. """
    assert (type(start_it) == int) and (type(n_its) == int)  # more assertions are done in pick_lambda_nhu()
    for it in range(start_it, start_it + n_its):
        _ = train_rbm(parameter_iter=it, verbose=1)
